module Data.BinaryHash
  ( hash
  , hashBase64Safe
  , hashBase64SafeByteString
  , hashBase64SafeText
  , hashUntyped
  , hashUntypedBase64Safe
  , hashUntypedBase64SafeByteString
  , hashUntypedBase64SafeText
  ) where

import           Crypto.Hash             (Blake2b_256, Digest)
import qualified Crypto.Hash             as C
import           Data.Binary             (Binary, encode)
import           Data.ByteArray          (convert)
import           Data.ByteArray.Encoding (Base (Base64URLUnpadded),
                                          convertToBase)
import           Data.ByteString         (ByteString)
import           Data.ByteString.Char8   (unpack)
import           Data.ByteString.Lazy    (toStrict)
import           Data.Text               (Text)
import           Data.Text.Encoding      (decodeUtf8)
import           Type.Reflection         (TypeRep, Typeable, typeOf)

withType :: Typeable a => a -> (a, TypeRep a)
withType x = (x, typeOf x)

-- | A hash of the argument together with its TypeRep. Returns 32
-- bytes of Blake2b_256 hash.
--
-- Note: the hash of TypeRep a can vary between different ways of
-- running programs. For example, it can be different between ghc and
-- ghci. To avoid this, we can used the "Untyped" variants, which hash
-- only the object, and not its type. The disadvantage is that we will
-- have a collision if we hash two objects with different types and
-- the same binary representation. (Such objects are quite common --
-- for example elements of different Enum types will often have the
-- same binary representation.)
hash :: (Binary a, Typeable a) => a -> ByteString
hash = hashUntyped . withType

-- | A hash of the argument together with its TypeRep. Returns 43
-- symbols which encode the Blake2b_256 hash in unpadded URL/Path-safe
-- base-64 encoding.
hashBase64Safe :: (Binary a, Typeable a) => a -> String
hashBase64Safe = hashUntypedBase64Safe . withType

hashBase64SafeByteString :: (Binary a, Typeable a) => a -> ByteString
hashBase64SafeByteString = hashUntypedBase64SafeByteString . withType

hashBase64SafeText :: (Binary a, Typeable a) => a -> Text
hashBase64SafeText = hashUntypedBase64SafeText . withType

-- | A hash of the argument, without its TypeRep. Returns 32 bytes of
-- Blake2b_256 hash.
hashUntyped :: Binary a => a -> ByteString
hashUntyped a = convert ((C.hash . toStrict . encode $ a) :: Digest Blake2b_256)

-- | A hash of the argument without its TypeRep. Returns 43 symbols
-- which encode the Blake2b_256 hash in unpadded URL/Path-safe base-64
-- encoding.
hashUntypedBase64Safe :: Binary a => a -> String
hashUntypedBase64Safe = unpack . hashUntypedBase64SafeByteString

-- | URL-encoded hash as a ByteString
hashUntypedBase64SafeByteString :: Binary a => a -> ByteString
hashUntypedBase64SafeByteString = convertToBase Base64URLUnpadded . hashUntyped

-- | URL-encoded hash as Text
hashUntypedBase64SafeText :: Binary a => a -> Text
hashUntypedBase64SafeText = decodeUtf8 . hashUntypedBase64SafeByteString
