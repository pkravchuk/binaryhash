# binaryhash

Simple Haskell hashing lib which uses `Blake2b_256` to hash 
things which are `Binary` and `Typeable`. Provides raw 
hash and unpadded URL/Path-safe base-64 encoding as defined in `RFC4648` section 5.